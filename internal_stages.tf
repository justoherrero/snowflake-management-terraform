resource "snowflake_stage" "internal_raw" {
  name     = "INTERNAL_RAW"
  database = var.database
  schema   = "LANDING_RAW"
}
