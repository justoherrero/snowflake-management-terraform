resource "snowflake_schema" "landing_raw" {
  database = var.database
  name     = "LANDING_RAW"
  comment  = "Schema defined to load raw data"

  is_transient        = false
  is_managed          = false
  data_retention_days = 10
}


resource "snowflake_schema" "data_transformation" {
  database = var.database
  name     = "DATA_TRANSFORMATION"
  comment  = "Schema defined to transform and enrich the raw data"

  is_transient        = false
  is_managed          = false
  data_retention_days = 10
}
