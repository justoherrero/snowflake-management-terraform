resource "snowflake_database" "terraform_database" {
  name                        = "TERRAFORM_DB"
  comment                     = "Database created and managed by Terraform"
  data_retention_time_in_days = 10
}
