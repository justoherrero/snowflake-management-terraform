provider "snowflake" {
  account  = var.account
  user     = var.username # required if not using profile or token. Can also be set via SNOWFLAKE_USER env var
  password = var.password

  // optional
  role      = var.role
  warehouse = var.warehouse
}
