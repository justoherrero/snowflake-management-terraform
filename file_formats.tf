resource "snowflake_file_format" "my_parquet" {
  name        = "MY_PARQUET"
  database    = var.database
  schema      = "LANDING_RAW"
  format_type = "PARQUET"
}
