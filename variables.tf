variable "account" {
  type    = string
  default = "cw58867.eu-central-1"
}

variable "username" {
  type    = string
  default = "TERRAFORM"
}

variable "password" {
  type = string
}

variable "role" {
  type    = string
  default = "SYSADMIN"
}

variable "warehouse" {
  type    = string
  default = "COMPUTE_WH_TERRAFORM"
}

variable "database" {
  type    = string
  default = "TERRAFORM_DB"
}
