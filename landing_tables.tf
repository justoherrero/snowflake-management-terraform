resource "snowflake_table" "yellow_taxi_trip" {
  database = var.database
  schema   = "LANDING_RAW"
  name     = "YELLOW_TAXI_TRIP"
  comment  = "Table with yellow taxi trips"

  column {
    name     = "SRC"
    type     = "VARIANT"
    nullable = false
  }

  column {
    name     = "LOAD_TIMESTAMP"
    type     = "TIMESTAMP"
    nullable = false
  }

  column {
    name     = "SOURCE_ID"
    type     = "STRING"
    nullable = false
  }
}
